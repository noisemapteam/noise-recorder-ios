//
//  MqqtClient.swift
//  Noise recorder
//
//  Created by Przemysław Kalawski on 19/05/2018.
//  Copyright © 2018 Wojciech Kumoń. All rights reserved.
//

import Foundation
import CocoaMQTT

class MqttClient: CocoaMQTTDelegate {
    
    var mqtt:CocoaMQTT?
    
    init(withClientId clientId:String, host:String, port:NSNumber) {
        mqtt = CocoaMQTT(
            clientID: clientId,
            host: host,
            port: UInt16(port.intValue)
        )
        mqtt?.delegate = self;
    }
    
    // Connect
    func connect(username:String, password:String) {
        mqtt?.username = username
        mqtt?.password = password
        let t = mqtt?.connect()
    }
    
    func connect() {
        let t = mqtt?.connect()
    }
    
    // Publish
    func publish(topic:String, message:String) {
        let message = CocoaMQTTMessage(
            topic: topic,
            string: message
        )
        
        mqtt?.publish(message)
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didConnectAck ack: CocoaMQTTConnAck) {
        print("Connect acknowledge.")
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didPublishMessage message: CocoaMQTTMessage, id: UInt16) {
        print("Publish: \(message.string!)")
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didPublishAck id: UInt16) {
        print("Publish acknowledge.")
    }

    func mqttDidPing(_ mqtt: CocoaMQTT) {
        print("Ping.")
    }
    
    func mqttDidReceivePong(_ mqtt: CocoaMQTT) {
        print("Pong.")
    }
    
    func mqttDidDisconnect(_ mqtt: CocoaMQTT, withError err: Error?) {
        print("Disconnect.")
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didReceiveMessage message: CocoaMQTTMessage, id: UInt16) {}
    func mqtt(_ mqtt: CocoaMQTT, didSubscribeTopic topic: String) {}
    func mqtt(_ mqtt: CocoaMQTT, didUnsubscribeTopic topic: String) {}
}
