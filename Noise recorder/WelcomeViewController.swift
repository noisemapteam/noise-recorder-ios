import UIKit
import AVFoundation
import CoreLocation
import MapKit
import SwiftMQTT

class WelcomeViewController: UIViewController, MQTTSessionDelegate, AVAudioRecorderDelegate, AVAudioPlayerDelegate, CLLocationManagerDelegate {
    func mqttDidReceive(message data: Data, in topic: String, from session: MQTTSession) {
        
    }
    
    var audioRecorder: AVAudioRecorder!
    var isAudioRecordingGranted: Bool!
    let meteringDelaySeconds: Double = 3.0
    let locationManager = CLLocationManager()
    var currentLocation : CLLocationCoordinate2D = CLLocationCoordinate2D()
    
    var mqttSession: MQTTSession!
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var channelTextField: UITextField!
    @IBOutlet weak var messageTextField: UITextField!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textView.text = nil
        establishConnection()
        
        NotificationCenter.default.addObserver(self, selector: #selector(WelcomeViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(WelcomeViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(WelcomeViewController.hideKeyboard))
        view.addGestureRecognizer(tapGesture)
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
        } catch let error as NSError {
            print(error.description)
        }
        setupLocationManager()
        checkRecordPermission()
        self.subscribeToChannel()
        startRecording()
        
    }
    
    func setupLocationManager(){
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    func checkRecordPermission() {
        switch AVAudioSession.sharedInstance().recordPermission() {
        case AVAudioSessionRecordPermission.granted:
            isAudioRecordingGranted = true
            break
        case AVAudioSessionRecordPermission.denied:
            isAudioRecordingGranted = false
            break
        case AVAudioSessionRecordPermission.undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        self.isAudioRecordingGranted = true
                    } else {
                        self.isAudioRecordingGranted = false
                    }
                }
            }
            break
        }
    }
    
    func startRecording() {
        let audioFilename = getDocumentsDirectory().appendingPathComponent("recording.m4a")
        
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 44100,
            AVNumberOfChannelsKey: 2,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        
        do {
            audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
            audioRecorder.delegate = self
            audioRecorder.isMeteringEnabled = true
            audioRecorder.prepareToRecord()
            audioRecorder.record()
            
            startMetrics()
            
        } catch {
            finishRecording()
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func finishRecording() {
        audioRecorder.stop()
        audioRecorder = nil
    }
    
    func startMetrics() {
        delay(meteringDelaySeconds) {
            self.audioRecorder.updateMeters()
            let averagePower = self.audioRecorder.averagePower(forChannel: 0)
            let positiveAverage = pow(10.0, averagePower / 20.0) * 120 // recalibrate here somehow
            print("average", positiveAverage)
            let peekPower = self.audioRecorder.peakPower(forChannel: 0)
            let positivePeek = pow(10.0, peekPower / 20.0) * 120 // recalibrate here somehow
            print("peek", positivePeek)
            self.postAction(peak: String(positivePeek))
            self.startMetrics()
        }
    }
    
    func postAction(peak: String){
        let position = Position(latitude: String(currentLocation.latitude), longitude: String(currentLocation.longitude))
        let noiseModel = NoiseModel(coords: position , timestamp: self.getCurrentDate(), dB: peak)
        let jsonEncoder = JSONEncoder()
        do {
            let jsonData = try jsonEncoder.encode(noiseModel)
            guard let jsonObject = try? JSONSerialization.jsonObject(with: jsonData, options: []) else {
                return
            }
            guard let httpBody = try? JSONSerialization.data(withJSONObject: jsonObject, options: []) else {
                return
            }
            
            mqttSession.publish(httpBody, in: "noiseMap/noiseData/krk", delivering: .atMostOnce, retain: false) {
                if !$0 {
                    self.appendStringToTextView("Error Occurred During Publish \($1)")
                    return
                }
                self.appendStringToTextView("Published \(httpBody.description) on channel \("/noiseMap/noiseData/krk")")
            }
            
        } catch {
            
        }
    }
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo = (notification as NSNotification).userInfo! as NSDictionary
        let kbHeight = (userInfo.object(forKey: UIKeyboardFrameBeginUserInfoKey) as! NSValue).cgRectValue.size.height
        bottomConstraint.constant = kbHeight
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        bottomConstraint.constant = 0
    }
    
    func establishConnection() {
        let host = "broker.mqttdashboard.com"
        let port: UInt16 = 1883
        let clientID = self.clientID()
        
        mqttSession = MQTTSession(host: host, port: port, clientID: clientID, cleanSession: true, keepAlive: 15, useSSL: false)
        mqttSession.delegate = self
        appendStringToTextView("Trying to connect to \(host) on port \(port) for clientID \(clientID)")
        
        mqttSession.connect {
            if !$0 {
                self.appendStringToTextView("Error Occurred During connection \($1)")
                return
            }
            self.appendStringToTextView("Connected.")
            self.subscribeToChannel()
        }
    }
    
    func subscribeToChannel() {
        let subChannel = "/noiseMap/noiseData/krk"
        mqttSession.subscribe(to: subChannel, delivering: .atMostOnce) {
            if !$0 {
                self.appendStringToTextView("Error Occurred During subscription \($1)")
                return
            }
            self.appendStringToTextView("Subscribed to \(subChannel)")
        }
    }
    
    func appendStringToTextView(_ string: String) {
        textView.text = "\(textView.text ?? "")\n\(string)"
        let range = NSMakeRange(textView.text.characters.count - 1, 1)
        textView.scrollRangeToVisible(range)
    }
    
    // MARK: - MQTTSessionDelegates
    func mqttSession(session: MQTTSession, received message: Data, in topic: String) {
        let string = String(data: message, encoding: .utf8)!
        appendStringToTextView("data received on topic \(topic) message \(string)")
    }
    
    func mqttSocketErrorOccurred(session: MQTTSession) {
        appendStringToTextView("Socket Error")
    }
    
    func mqttDidDisconnect(session: MQTTSession) {
        appendStringToTextView("Session Disconnected.")
    }

    // MARK: - Utilities
    
    func clientID() -> String {
        
        let userDefaults = UserDefaults.standard
        let clientIDPersistenceKey = "clientID"
        let clientID: String
        
        if let savedClientID = userDefaults.object(forKey: clientIDPersistenceKey) as? String {
            clientID = savedClientID
        } else {
            clientID = randomStringWithLength(5)
            userDefaults.set(clientID, forKey: clientIDPersistenceKey)
            userDefaults.synchronize()
        }
        
        return clientID
    }
    
    // http://stackoverflow.com/questions/26845307/generate-random-alphanumeric-string-in-swift
    func randomStringWithLength(_ len: Int) -> String {
        let letters = Array("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".characters)
        
        var randomString = String()
        for _ in 0..<len {
            let length = UInt32(letters.count)
            let rand = arc4random_uniform(length)
            randomString += String(letters[Int(rand)])
        }
        return String(randomString)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        currentLocation = locValue
    }
    
    private func getCurrentDate() -> String {
        let now = Date()
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "yyyy-MM-dd HH:mm"
        let dateString = formatter.string(from: now)
        return dateString
    }
    
    func delay(_ delay:Double, closure:@escaping ()->()) {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }
}



