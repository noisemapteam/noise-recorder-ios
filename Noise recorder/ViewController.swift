import UIKit
import AVFoundation
import CoreLocation
import MapKit
import SwiftMQTT

class ViewController: UIViewController, AVAudioRecorderDelegate, AVAudioPlayerDelegate, CLLocationManagerDelegate{

    var audioRecorder: AVAudioRecorder!
    var isAudioRecordingGranted: Bool!
    let meteringDelaySeconds: Double = 3.0
    let locationManager = CLLocationManager()
    var currentLocation : CLLocationCoordinate2D = CLLocationCoordinate2D()
    
    var mqttClient :MQTTSession!
    
    
    @IBOutlet weak var noiseLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
        } catch let error as NSError {
            print(error.description)
        }
        setupLocationManager()
        checkRecordPermission()
        startRecording()
    }
    
    func setupLocationManager(){
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    func checkRecordPermission() {
        switch AVAudioSession.sharedInstance().recordPermission() {
        case AVAudioSessionRecordPermission.granted:
            isAudioRecordingGranted = true
            break
        case AVAudioSessionRecordPermission.denied:
            isAudioRecordingGranted = false
            break
        case AVAudioSessionRecordPermission.undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        self.isAudioRecordingGranted = true
                    } else {
                        self.isAudioRecordingGranted = false
                    }
                }
            }
            break
        }
    }
    
    func startRecording() {
        let audioFilename = getDocumentsDirectory().appendingPathComponent("recording.m4a")
        
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        
        do {
            audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
            audioRecorder.delegate = self
            audioRecorder.isMeteringEnabled = true
            audioRecorder.prepareToRecord()
            audioRecorder.record()

            startMetrics()

        } catch {
            finishRecording()
        }
    }
    
    func finishRecording() {
        audioRecorder.stop()
        audioRecorder = nil
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func startMetrics() {
        delay(meteringDelaySeconds) {
            self.audioRecorder.updateMeters()
            let averagePower = self.audioRecorder.averagePower(forChannel: 0)
            let positiveAverage = averagePower + 120 // recalibrate here somehow
            print("average", positiveAverage)
            let peekPower = self.audioRecorder.peakPower(forChannel: 0)
            let positivePeek = peekPower + 120 // recalibrate here somehow
            print("peek", positivePeek)
            self.noiseLabel.text = String(positivePeek)
            self.postAction(peak: String(positivePeek))
            self.startMetrics()
        }
    }
    
    func postAction(peak: String){
        let url = String(format: "http://www.hivemq.com/demos/websocket-client/noiseMap/noiseData/krk")
        guard let serviceUrl = URL(string: url) else { return }
        let position = Position(latitude: "50.062011", longitude: "19.943860")
        let noiseModel = NoiseModel(coords: position , timestamp: "2018-04-14T14:16:48+00:00", dB: peak)
        let jsonEncoder = JSONEncoder()
        do {
            let jsonData = try jsonEncoder.encode(noiseModel)
            var request = URLRequest(url: serviceUrl)
            request.httpMethod = "POST"
            request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            guard let jsonObject = try? JSONSerialization.jsonObject(with: jsonData, options: []) else {
                return
            }
            guard let httpBody = try? JSONSerialization.data(withJSONObject: jsonObject, options: []) else {
                return
            }
 
            mqttClient.publish(httpBody, in: "/noiseMap/noiseData/krk", delivering: .atLeastOnce, retain: false) { (succeeded, error) -> Void in
                if succeeded {
                    print("Published!")
                }
            }
    
        } catch {
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        currentLocation = locValue
    }
    
    private func getCurrentDate() -> String {
        let now = Date()
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "yyyy-MM-dd HH:mm"
        let dateString = formatter.string(from: now)
        return dateString
    }
}

func delay(_ delay:Double, closure:@escaping ()->()) {
    let when = DispatchTime.now() + delay
    DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
}
