//
//  NoiseModel.swift
//  Noise recorder
//
//  Created by Przemysław Kalawski on 17/05/2018.
//  Copyright © 2018 Wojciech Kumoń. All rights reserved.
//

import Foundation
import CoreLocation

class NoiseModel: Codable {
    
    var coords : Position
    var timestamp : String
    var dB : String
    
    private enum CodingKeys: String, CodingKey {
        case coords
        case timestamp
        case dB
    }
    
    init(coords: Position, timestamp: String, dB : String) {
        self.coords = coords
        self.timestamp = timestamp
        self.dB = dB
    }
    
    required init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        coords = try values.decode(Position.self, forKey: .coords)
        timestamp = try values.decode(String.self, forKey: .timestamp)
        dB = try values.decode(String.self, forKey: .dB)
    }
    
    func encode(to encoder: Encoder) throws {
        do {
            var container = encoder.container(keyedBy: CodingKeys.self)
            try container.encode(coords, forKey: .coords)
            try container.encode(timestamp, forKey: .timestamp)
            try container.encode(dB, forKey: .dB)
        } catch {
            print(error)
        }
    }
}

