//
//  Position.swift
//  Noise recorder
//
//  Created by Przemysław Kalawski on 17/05/2018.
//  Copyright © 2018 Wojciech Kumoń. All rights reserved.
//

import Foundation

class Position: Codable {
    var latitude : String
    var longitude : String
    
    init(latitude: String, longitude: String) {
        self.latitude = latitude
        self.longitude = longitude
    }
}
